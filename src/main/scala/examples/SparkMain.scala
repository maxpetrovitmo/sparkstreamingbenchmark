package examples

import java.lang.System.currentTimeMillis
import java.net.Socket
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeUnit.MILLISECONDS
import java.util.concurrent.atomic.AtomicLong
import java.util.concurrent.locks.Lock

import com.codahale.metrics.{Gauge, MetricRegistry}
import com.mongodb.client.MongoCollection
import org.apache.hadoop.io.{LongWritable, Text}
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat
import org.apache.spark._
import org.apache.spark.rdd.RDD
import org.apache.spark.scheduler.SparkListener
import org.apache.spark.streaming.kafka.KafkaUtils
import org.apache.spark.streaming.{Minutes, Seconds, StateSpec, StreamingContext}
import org.bson.Document
import org.joda.time.DateTime
import org.apache.spark.streaming._

import scala.util.Random

/**
  * Created by vipmax on 20.09.16.
  */
object SparkMain {
//  ./bin/spark-submit --class examples.Main --master spark://node-92-50:7077 --executor-memory 2G --total-executor-cores 20 SparkStreamingKafkaExample-1.0-SNAPSHOT-jar-with-dependencies.jar
  val appname = "SparkKafkaWordCount"
  val graphiteRootFolder = appname/*+ "-" + currentTimeMillis()*/

  def main(args: Array[String]) {
    
    val (zkQuorum, group, topics, numThreads) = ("192.168.13.133:2181", "group","test", "1")
        val sparkConf = new SparkConf().setMaster("local[2]").setAppName("KafkaWordCountLocal")
      .set("spark.metrics.conf","/home/vipmax/IdeaProjects/SparkStreamingKafkaExample/metrics.properties")
//    val sparkConf = new SparkConf().setAppName(appname)
    val sc = new SparkContext(sparkConf)

    val ssc = new StreamingContext(sc, Seconds(2))

    /* sudo -u hdfs hadoop fs -chmod 777 /tmp */
//    ssc.checkpoint("hdfs://192.168.92.50:8020/tmp")
    ssc.checkpoint(".")

    val topicMap = topics.split(",").map((_, numThreads.toInt)).toMap

//    val lines = KafkaUtils.createStream(ssc, zkQuorum, group, topicMap).map(_._2)
//    val lines = ssc.textFileStream("hdfs://192.168.92.50:8020/tmp")
//    val lines = ssc.textFileStream("hdfs://192.168.92.50:8020/tmp/")
    //    val lines = ssc.fileStream[LongWritable, Text, TextInputFormat]("hdfs://192.168.92.50:8020/tmp")

//    work
//    val lines = ssc.fileStream[LongWritable, Text, TextInputFormat]("file:///home/vipmax/IdeaProjects/SparkStreamingKafkaExample/")

//    val lines = ssc.socketTextStream("192.168.0.118", 8007)
    val lines = ssc.socketTextStream("192.168.13.133", 8007)



    val words = lines.flatMap { line =>
//      println(line)
      line.split(" ")
    }

//    val words = lines.flatMap { line =>
//      println(line._2.toString)
//      line._2.toString.split(" ")
//    }



    val wordCounts = words.map { word =>
      println(word)
      (word, 1)
    }
      .map(MapClass.map)
//      .reduceByWindow((tuple: (String, Int), tuple0: (String, Int)) => {
//          println(s"reducing ${tuple._1} ${tuple0._1} vals:{${tuple._2}, ${tuple0._2}" )
//          (tuple._1, tuple._2 + tuple0._2)
//        }
//        , windowDuration = Minutes(1), slideDuration = Seconds(10))
      .reduceByKeyAndWindow(
        reduceFunc = (l, l1) => l + l1,
        invReduceFunc = (l, l1) => l - l1,
        windowDuration = Minutes(1),
        slideDuration = Seconds(10),
        numPartitions = 2
      )

      wordCounts.foreachRDD((rdd: RDD[(String, Int)]) => Unit)
//    wordCounts.saveAsTextFiles("spark/out")



    ssc.start()
    ssc.awaitTermination()
  }

  object MapClass extends java.io.Serializable {
    var counter: AtomicLong = new AtomicLong(0)

//    @transient
//    var lock:Object = new Object()

    @transient
    var socket: Option[Socket] = None

    @transient
    var collection: Option[MongoCollection[Document]] = None


    def map(tup:(String, Int)): (String, Int) = {
//      lock.synchronized {
        counter getAndAdd tup._1.getBytes("UTF-8").length
//        println("counter = " + counter)
//      }

      val (word, one) = tup

      val data = new StringBuilder()
        .append(graphiteRootFolder)
        .append(".")
        .append("customMetric")
        .append(" ")
        .append(counter)
        .append(" ")
        .append(MILLISECONDS.toSeconds(currentTimeMillis()))
        .append("\n")
        .toString()
        .getBytes()

      val s = socket match {
        case Some(sock) => sock
        case _ =>
          socket = Option(new Socket("192.168.13.133", 2003))
          socket.get
      }



      s.getOutputStream()
        .write(data)

//      val c = collection match {
//                case Some(coll) => coll
//                case _ =>
//                  collection = Option(new com.mongodb.MongoClient("192.168.13.133").getDatabase("Benchmarks").getCollection("Spark_words_count"))
//                  collection.get
//              }
//
//
//      c.insertOne(new Document("count", counter).append("time", MILLISECONDS.toSeconds(currentTimeMillis())))

      (word, one)
    }
  }

}

