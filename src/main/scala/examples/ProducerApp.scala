package examples

import java.io.{BufferedWriter, FileWriter, OutputStreamWriter, PrintWriter}
import java.net.ServerSocket

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.kafka.ProducerSettings
import akka.kafka.scaladsl.Producer
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Source
import com.typesafe.config.ConfigFactory
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.{ByteArraySerializer, StringSerializer}

import scala.util.Random
/**
  * Created by vipmax on 23.09.16.
  */

/* deps: akka-stream_2.11, akka-stream-kafka_2.11, mongo-java-driver*/
object ProducerApp extends App {
//  val customConf = ConfigFactory.parseString("""
//                                               akka {
//                                                 loglevel = "DEBUG"
//                                               }
//                                             """)
//
//  implicit val actorSystem = ActorSystem("akka", ConfigFactory.load(customConf))
//  implicit val materializer = ActorMaterializer()

//  val producerSettings = ProducerSettings(actorSystem, new ByteArraySerializer, new StringSerializer).withBootstrapServers("192.168.13.133:9092")
  val fw = new FileWriter("data100mb.txt", true)

//  val socket = new ServerSocket(8007)
//  val socketWriter: PrintWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.accept.getOutputStream)))

  val isKafka = false
  val isSocket = false
  val isFile = true

  var bytesCounter = 0
  var linesCounter = 0

  while (true) { try {
    println("Starting")
    val it = new com.mongodb.MongoClient("192.168.13.133").getDatabase("VkFest").getCollection("data").find().iterator()



    while (it.hasNext) {
      val data = it.next().getString("text").replace("\n", " ")

      bytesCounter += data.getBytes("UTF-8").length
      linesCounter += 1

      if(isSocket) {
//        socketWriter.println(data)
//        socketWriter.flush()
      }

      if(isFile) {
        fw.write(data + " \n")
        fw.flush()
      }

      if(isKafka) {
//          Source(List(data))
//            .map { data => new ProducerRecord[Array[Byte], String]("test", data) }
//            .runWith(Producer.plainSink(producerSettings))
      }


      if(bytesCounter / (1024 * 1024) >= 100)
        System.exit(0)


      println(s"$bytesCounter bytes, ${bytesCounter / 1024} kb, ${bytesCounter / (1024 * 1024)} mb, $linesCounter lines")
//      Thread.sleep(Random.nextInt(10))
    }
  } catch {case e:Exception => }}
}

