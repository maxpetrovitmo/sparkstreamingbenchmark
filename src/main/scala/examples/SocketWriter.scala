package examples
import java.net.{ServerSocket, Socket}
import java.io._

import scala.io.Source
/**
  * Created by vipmax on 28.09.16.
  */
object SocketWriter {
  /* java -cp SparkStreamingKafkaExample-1.0-SNAPSHOT-allinone.jar  examples.SocketWriter 8007 data1mb.txt */
  def main(args: Array[String]) {
    val port = if(args.length > 1) args(0).toInt else 8007
    val file = if(args.length > 1) args(1) else "data1mb.txt"

    val data = Source.fromFile(file).getLines().toList.mkString(" ")
    val socket = new ServerSocket(port)
    println("Waiting for connections")
    val writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.accept.getOutputStream)))
    println("Connected")

    while (true) {
      writer.println(data)
      writer.flush()
      println("Sended " + file )
      Thread.sleep(1000)
    }
  }
}
