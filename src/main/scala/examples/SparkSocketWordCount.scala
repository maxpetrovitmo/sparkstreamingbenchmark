package examples

import org.apache.hadoop.io.{LongWritable, Text}
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by vipmax on 28.09.16.
  */
object SparkSocketWordCount {
  def main(args: Array[String]) {

    val sparkConf = new SparkConf().setMaster("local[2]").setAppName("HdfsWordCount")
    val sc = new SparkContext(sparkConf)
    val ssc = new StreamingContext(sc, Seconds(2))

    //    val inputDStream = ssc.fileStream[LongWritable, Text, TextInputFormat]("hdfs://192.168.92.50:8020/tmp")
//    val inputDStream = ssc.fileStream[LongWritable, Text, TextInputFormat]("file:///home/vipmax/IdeaProjects/SparkStreamingKafkaExample/data2.txt")

    val inputDStream = ssc.socketTextStream("192.168.0.118", 8007)
    val words = inputDStream.flatMap{ line => line.split(" ") }
    val wordCounts = words.map(x => (x, 1)).reduceByKey(_ + _)
    wordCounts.print()
    ssc.start()
    ssc.awaitTermination()
  }
}
