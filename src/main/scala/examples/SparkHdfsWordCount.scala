package examples

import org.apache.hadoop.io.{LongWritable, Text}
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.streaming.{Seconds, StreamingContext}

/**
  * Created by vipmax on 28.09.16.
  */
object SparkHdfsWordCount {
  def main(args: Array[String]) {

    val sparkConf = new SparkConf().setMaster("local[2]").setAppName("HdfsWordCount")
    val sc = new SparkContext(sparkConf)
    val ssc = new StreamingContext(sc, Seconds(2))

//    val lines = ssc.fileStream[LongWritable, Text, TextInputFormat]("hdfs://192.168.92.50:8020/tmp")
    val lines = ssc.fileStream[LongWritable, Text, TextInputFormat]("file:///home/vipmax/IdeaProjects/SparkStreamingKafkaExample/data2.txt")
    val words = lines.flatMap{line =>
      println(line)
      line._2.toString.split(" ")
    }
    val wordCounts = words.map(x => (x, 1)).reduceByKey(_ + _)
    wordCounts.saveAsTextFiles("spark/out.txt")
    ssc.start()
    ssc.awaitTermination()
  }
}
